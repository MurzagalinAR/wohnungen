## 07.03

### 17.15
Steinmetzstr. 39b, 10783 Berlin. Schoneberg

https://www.ado.immo/exposes/1-zimmer-wohnung-mit-hervorragender-anbindung-sowie-ebk/


## 08.03

### 15.30
Frankfurter Allee 82, 10247 Berlin, Friedrichshain (Friedrichshain)

https://www.immobilienscout24.de/expose/103443605

## 09.03

### 13.00
Wilhelmstraße 5, 10963 Berlin-Kreuzberg, Kreuzberg (Kreuzberg)

https://www.immobilienscout24.de/expose/103481090#/

## 10.03

### 11.00
Sonnenallee 66, 12045 Berlin
https://www.immobilienscout24.de/expose/103305915#/

## 12.03

### 16.15

Gerhardtstraße 3, 10557 Berlin